### A Survey of MWE Identification Experiments: The Devil is in the Details
_Supplementary material submitted with paper_
* Carlos Ramisch, Abigail Walsh, Thomas Blanchard, and Shiva Taslimipoor

* `significance.py` - Main script, runs statistical significance testing for two systems `--pred` and `--comp` using bootstrap. Generates 10k samples so may take some time if test set is large.
* `runSignificanceAllPARSEME1.2.sh` - Calculate statistical significance using bootstrap for all system pairs in each track of PARSEME 1.2 shared task
* `Full-significance-PARSEME1.2-results-RAW.txt` - Result of `runSignificanceAllPARSEME1.2.sh`
* `convertSignificanceToCSV.sh` - Convert file above to more friendly CSV format
* `Full-significance-PARSEME1.2-results.csv` - Result of `convertSignificanceToCSV.sh`

In order to run the experiments, please copy the PARSEME shared task 1.2 corpus directory into a folder called `sharedtask-data` containing one subfolder per language using its 2-letter code (e.g. `ZH` for Chinese) and two files `test.cupt` with the gold test corpus and `train.cupt` with the trainig corpus. This can be obtained from the [shared task git repo](https://gitlab.com/parseme/sharedtask-data/-/tree/master/1.2/).

Also please place the system predictions into a folder called `system-results`, one subfolder per system, then one subfolder per language, then a file named `test.system.cupt`, for instance, `system-results/TRAVIS-multi.open/ZH/test.system.cupt`.

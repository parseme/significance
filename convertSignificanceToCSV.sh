filename="Full-significance-PARSEME1.2-results-RAW.txt"

dict = {}

file = open(filename,'r')

content = file.read().split('\n')
#print(content)
for lines in content :
    if lines[:8] == 'Language' : # récupération de la langue
        L = lines[-2:]
        if lines[-2:] not in dict.keys() :
            dict[lines[-2:]] = {'open':{},'closed':{}}
    
    if lines[:7]=='Systems' :
        words = lines.split()
        S = words[-1]
        S1,S2 = S.split(',')
        if lines[-6:]=='closed' :
            type_of_system = 'closed'
        else :
            type_of_system = 'open'
    
    if lines[:8]=='* (pred)' :
        content_1 = lines.split()
        if content_1[3] == ':' :
            ind = 0
            Metric = content_1[2]
        else :
            ind = -1
            Metric = content_1[2][:-1]

        if Metric not in dict[L][type_of_system].keys() :
            dict[L][type_of_system][Metric] = {}
        dict[L][type_of_system][Metric][S1+',Precision'] = float(content_1[4+ind].split('=')[1][:])
        dict[L][type_of_system][Metric][S1+',Recall'] = float(content_1[5+ind].split('=')[1][:])
        dict[L][type_of_system][Metric][S1+',F1'] = float(content_1[6+ind][:])
    
    if lines[:8]=='* (comp)' :
        content_1 = lines.split()
        if content_1[3] == ':' :
            ind = 0
            Metric = content_1[2]
        else :
            ind = -1
            Metric = content_1[2][:-1]
        if Metric not in dict[L][type_of_system].keys() :
            dict[L][type_of_system][Metric] = {}
        dict[L][type_of_system][Metric][S2+',Precision'] = float(content_1[4+ind].split('=')[1][:])
        dict[L][type_of_system][Metric][S2+',Recall'] = float(content_1[5+ind].split('=')[1][:])
        dict[L][type_of_system][Metric][S2+',F1'] = float(content_1[6+ind][:])

    if lines[:1]=='p' :
        content_1 = lines.split('=')
        dict[L][type_of_system][Metric][S+',Precision'] = float(content_1[2][:-3])
        dict[L][type_of_system][Metric][S+',Recall'] = float(content_1[3][:-4])
        dict[L][type_of_system][Metric][S+',F1'] = float(content_1[4][:-1])

#print(dict)


file.close()

import csv

languages=["DE","EL","EU","FR","GA","HE","HI","IT","PL","PT","RO","SV","TR","ZH"]
systems_closed=["ERMI.closed","Seen2Seen.closed"]
systems_open=["FipsCo.open","HMSid.open","MTLB-STRUCT.open","Seen2Unseen.open","TRAVIS-mono.open","TRAVIS-multi.open"]

with open("Results.csv",'w',newline='') as f :
    writer = csv.writer(f)
    writer.writerows([['Language','Type of system','Metric','System 1','System 2', 'Type of results','Results']])
    for L in dict.keys() :
        for T in dict[L].keys() :
            for M in dict[L][T].keys() :
                for S in dict[L][T][M] :
                    if len(S.split(',')) == 2:
                        S1,S3 = S.split(',')
                        S2 = ''
                    if len(S.split(',')) == 3:
                        S1,S2,S3 = S.split(',')
                    writer.writerows([[L,T,M,S1,S2,S3,str(dict[L][T][M][S])]])


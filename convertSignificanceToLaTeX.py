import pandas
import numpy as np

############ PARAMETERS

Language = 'FR'                         #DE EL EU FR GA HE HI IT PL PT RO SV TR ZH
#type_of_system = 'open'                 #open closed 
metric = 'Unseen-in-train'                    #MWE-based Continuous Discontinuous Multi-token Single-token Seen-in-train Unseen-in-train Variant-of-train Identical-to-train
type_of_results = 'F1'                  #Precision Recall F1



############ MAIN

systems_closed=["ERMI.closed","Seen2Seen.closed"]
systems_open=["FipsCo.open","HMSid.open","MTLB-STRUCT.open","Seen2Unseen.open","TRAVIS-mono.open","TRAVIS-multi.open"]
systems = [systems_closed,systems_open]
types_of_system = ['closed','open']

def name_of_syst(str_syst) :
    return str_syst.split('.')[0]


def dict_to_latex_table(data):
    n = len(data)
    title = "p-value of the the \\textit{"+metric+"} \\textit{" + type_of_results +"} score for results on \\textit{"+metric+"} MWEs in \\textit{" + Language +"}" 

    table = "\\begin{table*}[htb] \n\centering\n \small\n"


    length = "|c|" + (n-1)*"c|"+"|c|"
    table += "\\begin{tabular}{"+length+"}\n\hline\n"
    table += "\multirow{2}{*}{Systems} & \multicolumn{" + str(n-1) + "}{c||}{Open track} & Closed track \\\\ \n"

    turn = 0
    for key in data.keys() :
        if turn < 1:
            for key2 in data[key].keys() :
                table += " & "+str(key2)
            turn += 1
    
    for key2 in data[key].keys() :
        table += " & " + str(key2)
    table += " \\\\ \n\hline\n"

    turn = 0
    for key1 in data.keys():
        if turn == n-1 :
            table += "\hline\n"
            espace = "-"
        else :
            espace = " "
        table += str(key1) + " " + turn*("&"+espace)
        turn += 1
        for key2 in data[key1].keys() :
            table += "& " + str(data[key1][key2]) + " "
        if turn <= n-1 :
            table += "& - \\\\ \hline\n"
        else :
            table += " \\\\ \hline\n"
    table += "\n\\end{tabular}\n"
    table += "\caption{"+ title +"}\n\end{table*}"
    return table

def dict_to_latex_table_F_score(data,score):
    n = len(data)+1
    title = "p-value of the the \\textit{"+metric+"} \\textit{" + type_of_results +"} score for results on \\textit{"+metric+"} MWEs in \\textit{" + Language +"}" 

    table = "\\begin{table*}[htb] \n\centering\n \small\n"


    length = "|c|" + (n-1)*"c|"+"|c|"
    table += "\\begin{tabular}{"+length+"}\n\hline\n"
    table += "\multirow{3}{*}{Systems} & \multicolumn{" + str(n-1) + "}{c||}{Open track} & Closed track \\\\ \n &"

    turn = 0
    for key in data.keys() :
        if turn < 1:
            for key2 in data[key].keys() :
                table += " & "+str(key2)
            turn += 1
    
    for key2 in data[key].keys() :
        table += " & " + str(key2)
    table += " \\\\ \n\hline\n & "+type_of_results

    turn = 0
    for key in scores :
        if turn == 0 or turn == n-1:
            turn += 1
        else :
            table += " & \\textbf{"+str(scores[key])+"}"
            turn += 1
    
    table += " \\\\ \n\hline\n"

    turn = 0
    for key1 in data.keys():
        if turn == n-2 :
            table += "\hline\n"
            espace = "-"
        else :
            espace = " "
        table += str(key1) + " & \\textbf{" + str(scores[key1]) + "} " + turn*("&"+espace)
        turn += 1
        for key2 in data[key1].keys() :
            table += "& " + str(data[key1][key2]) + " "
        if turn <= n-2 :
            table += "& - \\\\ \hline\n"
        else :
            table += " \\\\ \hline\n"
    table += "\n\\end{tabular}\n"
    table += "\caption{"+ title +"}\n\end{table*}"
    return table

results = pandas.read_csv('Full-significance-PARSEME1.2-results.csv')
#print(results.keys()) 

def recup_data() :
    data = {}
    scores = {}
    # We get the first dictionnary
    for index, line in results.iterrows() :
        if line['Language'] == Language and line['Type of system'] == 'open' and line['Metric'] == metric :
            if line['System 2'] in systems[types_of_system.index('open')] :
                if line['Type of results'] == type_of_results :
                    syst1 = name_of_syst(line['System 1'])
                    syst2 = name_of_syst(line['System 2'])
                    R = line['Results']
                    if syst1 not in data.keys() :
                        data[syst1] = {}
                    data[syst1][syst2] = R

        if line['Language'] == Language and line['Type of system'] == 'closed' and line['Metric'] == metric :
            if line['System 2'] in systems[types_of_system.index('closed')] :
                if line['Type of results'] == type_of_results :
                    syst1 = name_of_syst(line['System 1'])
                    syst2 = name_of_syst(line['System 2'])
                    R = line['Results']
                    if syst1 not in data.keys() :
                        data[syst1] = {}
                    data[syst1][syst2] = R

        # We get the order by saving the F-score
        if line['Language'] == Language and line['Type of system'] == 'open' and line['Metric'] == metric :
            if line['System 2'] not in systems[types_of_system.index('open')] :
                if line['Type of results'] == type_of_results :
                    syst1 = name_of_syst(line['System 1'])
                    R = line['Results']
                    scores[syst1] = R
        
        if line['Language'] == Language and line['Type of system'] == 'closed' and line['Metric'] == metric :
            if line['System 2'] not in systems[types_of_system.index('closed')] :
                if line['Type of results'] == type_of_results :
                    syst1 = name_of_syst(line['System 1'])
                    R = line['Results']
                    scores[syst1] = R

    return data, scores


def order_data(data,scores) :
    l_scores = sorted(scores.items(), key=lambda x: x[1], reverse = True)
    scores = {}
    for s in l_scores :
        if s[0]+".closed" not in systems[types_of_system.index('closed')] :
            scores[s[0]] = s[1]
    
    for s in l_scores :
        if s[0]+".closed" in systems[types_of_system.index('closed')] :
            scores[s[0]] = s[1]

    new_data = {}
    turn = 0
    for key in scores :
        if key+".closed" not in systems[types_of_system.index('closed')] and turn < len(scores)-3:
            new_data[key] = {}
            turn += 1
    turn = 0
    for key in scores :   
        if key+".closed" in systems[types_of_system.index('closed')] and turn < 1:
            new_data[key] = {}
            turn += 1
    
    turn_d = 0
    for key1 in new_data :
        turn_s = 0
        for key2 in scores :
            if turn_s <= turn_d :
                turn_s += 1
            else :
                if key1 in data.keys() :
                    if key2 in data[key1].keys() :
                        new_data[key1][key2] = data[key1][key2]
                if key2 in data.keys() :
                    if key1 in data[key2].keys() :
                        new_data[key1][key2] = data[key2][key1]
        turn_d += 1
    return new_data,scores



data,scores = recup_data()
data,scores = order_data(data,scores)
print(dict_to_latex_table_F_score(data,scores))


#!/bin/bash

languages=(DE EL EU FR GA HE HI IT PL PT RO SV TR ZH)
systems_closed=(ERMI.closed Seen2Seen.closed)
systems_open=(FipsCo.open HMSid.open MTLB-STRUCT.open Seen2Unseen.open TRAVIS-mono.open TRAVIS-multi.open)

nb_languages=${#languages[@]}
nb_systems_closed=${#systems_closed[@]}-1
nb_systems_open=${#systems_open[@]}-1

outfile="Full-significance-PARSEME1.2-results-RAW.txt"

for ((i=0;i<nb_languages;i++)); 
do  
    for ((j=0;j<nb_systems_closed;j++)); 
    do
        echo "Language : ${languages[i]}" >> ${outfile}
        echo "Systems : ${systems_closed[j]},${systems_closed[j+1]}" >> ${outfile}
        python ./significance.py --gold sharedtask-data/${languages[i]}/test.cupt --pred system-results/${systems_closed[j]}/${languages[i]}/test.system.cupt --comp system-results/${systems_closed[j+1]}/${languages[i]}/test.system.cupt --train preliminary-sharedtask-data/${languages[i]}/train.cupt | sed -r 's/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g'>> ${outfile};
        echo >> ${outfile}
        echo >> ${outfile}
    done
    echo >> ${outfile}

    for ((j=0;j<nb_systems_open;j++)); 
    do  
        for ((k=j;k<nb_systems_open;k++)); 
        do
            echo "Language : ${languages[i]}" >> ${outfile}
            echo "Systems : ${systems_open[j]},${systems_open[k+1]}" >> ${outfile}
            python ./significance.py --gold preliminary-sharedtask-data/${languages[i]}/test.cupt --pred system-results/${systems_open[j]}/${languages[i]}/test.system.cupt --comp system-results/${systems_open[k+1]}/${languages[i]}/test.system.cupt --train preliminary-sharedtask-data/${languages[i]}/train.cupt | sed -r 's/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g'>> ${outfile};
            echo >> ${outfile}
            echo >> ${outfile}
        done
    done
    echo >> ${outfile}
done

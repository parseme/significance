#! /usr/bin/env python3
from typing import Union, NamedTuple, Dict, Set, FrozenSet, Iterable, Optional,\
                   Callable, List, cast, TextIO, Tuple, Deque
import argparse
import collections
import sys
from conllu import TokenList, parse_incr
import parseme.cupt as cupt
#import parseme.cupteval as cupteval #TODO move all eval functionalities here
import random

import pdb # TODO removeme in final version

#TODO add documentation to all functions
#TODO transform these functions into a library-like set of functions cupteval.py

DEFAULT_SIGNIF_LEVEL=0.05
DEFAULT_BOOTSTRAP_N=1000

parser = argparse.ArgumentParser(description="Evaluate the predictions of an \
MWE identifier vs a gold file (and obtain p-value wrt to another prediction).",  formatter_class=argparse.ArgumentDefaultsHelpFormatter)
#parser.add_argument("--debug", action="store_true",
#        help="""Print extra debugging information (you can grep it,
#        and should probably pipe it into `less -SR`)""")
#parser.add_argument("--combinatorial", action="store_true",
#        help="""Run O(n!) algorithm for weighted bipartite matching.
#        You should probably not use this option""")
# TODO: add
parser.add_argument("--gold", metavar="GOLD", dest="gold_file",
        required=True, type=argparse.FileType('r', encoding='UTF-8'), default=argparse.SUPPRESS,
        help="""The reference gold-standard annotated file, in CUPT""")
parser.add_argument("--pred", metavar="PRED",
        dest="pred_file", required=True, default=argparse.SUPPRESS,
        type=argparse.FileType('r', encoding='UTF-8'),
        help="""System prediction file to evaluate, in CUPT""")
parser.add_argument("--train", metavar="TRAIN", dest="train_file",
        required=False, type=argparse.FileType('r', encoding='UTF-8'),
        help="""Training file to get seen/unseen MWE stats, in CUPT""")
parser.add_argument("--comp", metavar="COMP",
        dest="comp_file", required=False,
        type=argparse.FileType('r', encoding='UTF-8'),
        help="""Prediction to test for significant difference, in CUPT""")
parser.add_argument("--signif-level", required=False, type=float,
        default=DEFAULT_SIGNIF_LEVEL, metavar="ALPHA", dest="signif_level",
        help="""Consider p-value significant if below this threshold""")
parser.add_argument("--bootstrap-samples", required=False, type=int,
        default=DEFAULT_BOOTSTRAP_N, metavar="N", dest="bootstrap_samples",
        help="""Number of bootstrap samples created with replacement""")


######################################################################
######################################################################

class ResultPRF:
    """
    TODO: add doc
    """

    countertype: str
    precision: float
    recall: float
    f1: float

    def __init__(self, precision: float = 0.0, recall: float = 0.0,
                 f1: float = 0.0):
        """
        TODO: add doc
        """
        self.precision = precision
        self.recall = recall
        self.f1 = f1

######################################################################

    def calculate_p_r_f(self, match: 'MatchCounter'):
        """
        TODO: add doc
        """
        self.precision = match.n_true_positive / (match.n_positive or 1)
        self.recall = match.n_true_positive / (match.n_true or 1)
        if self.precision :
            self.f1 = (2 * self.precision * self.recall) \
                      / (self.precision + self.recall)
        else:
            self.f1 = 0.0

######################################################################

    def __getitem__(self, index:int) -> float:
        """
        TODO: add doc
        """
        return list(self)[index]

######################################################################

    def __sub__(self, other: 'ResultPRF') -> 'ResultPRF':
        """
        TODO: add doc
        """
        return ResultPRF( self.precision - other.precision,
                          self.recall - other.recall,
                          self.f1 - other.f1)

######################################################################
    def __iter__(self) :
        """
        TODO: add doc
        """
        for elem in [self.precision, self.recall, self.f1]:
            yield elem

######################################################################

    def __repr__(self) -> str:
        """
        TODO: add doc
        """
        return "(P={self.precision:.4f}, R={self.recall:.4f}, " \
               "F1={self.f1:.4f})".format(self=self)

######################################################################

    def row_spacing(self, prefix:str, prec:str, rec: str, fmeas:str) -> str:
      return "* {pre:20s}: {p:30s} {r:30s} {f:30s}".format(pre=prefix,p=prec,r=rec,f=fmeas)
      
######################################################################

    def colour_pr(self, numer:int, denom:int, ratio: float) -> str:
      return  "\033[37m{n}/{d}\033[0m=\033[1m{r:.4f}\033[0m".format(n=numer,d=denom,r=ratio)

######################################################################

    def str_detail(self, match:'MatchCounter', prefix:str="") -> str:
        if prefix : 
            pre = "({prefix:4s}) ".format(prefix=prefix)
        else :
            pre = ""
        return self.row_spacing("{p}{m.countertype}".format(p=pre, m=match),
                                self.colour_pr(match.n_true_positive, match.n_positive, self.precision),
                                self.colour_pr(match.n_true_positive, match.n_true, self.recall),
                                "\033[1m{f:.4f}\033[0m".format(f=self.f1)
                                )
        #return "* {p}{m.countertype:13s}: " \
        #          "\033[37m{m.n_true_positive}/{m.n_positive}\033[0m"\
        #          "=\033[1m{self.precision:.4f}\033[0m " \
        #          "{m.n_true_positive}/{m.n_true}"\
        #          "=\033[1m{self.recall:.4f}\033[0m " \
        #          "\033[1m{self.f1:.4f}\033[0m".format(self=self, p=pre, m=match)

######################################################################
######################################################################

class MatchCounter:
    """
    TODO: add doc
    """

    countertype: str
    n_true: int
    n_positive: int
    n_true_positive: int

    def __init__(self, countertype: str, n_true: int = 0,
                 n_positive: int = 0, n_true_positive: int = 0) -> None:
        """
        TODO: add doc
        """
        self.countertype = countertype
        self.n_true = n_true
        self.n_positive = n_positive
        self.n_true_positive = n_true_positive

######################################################################

    def __repr__(self) -> str:
        return "{}(T={}, P={}, TP={})".format(self.countertype, self.n_true,
                                              self.n_positive,
                                              self.n_true_positive)

######################################################################

    def __bool__(self) -> bool:
        """
        TODO: add doc
        """
        # There can't be any true_positive if there's no true and no positive
        # No need to test n_true_positive in this case ;-)
        return self.n_true != 0 or self.n_positive != 0

######################################################################

    def __eq__(self, other: object) -> bool:
        """
        TODO: add doc
        """
        return type(other) == MatchCounter and \
               self.n_true == cast(MatchCounter, other).n_true and \
               self.n_positive == cast(MatchCounter, other).n_positive and \
               self.n_true_positive == cast(MatchCounter, other).n_true_positive

######################################################################

    def incr(self, i_true: int, i_positive: int, i_true_positive: int):
        """
        TODO: add doc
        """
        self.n_true += i_true
        self.n_positive += i_positive
        self.n_true_positive += i_true_positive

######################################################################

    def __iadd__(self, other: 'MatchCounter') -> 'MatchCounter':
        """
        TODO: add doc
        """
        self.n_true += other.n_true
        self.n_positive += other.n_positive
        self.n_true_positive += other.n_true_positive
        return self

######################################################################

    def get_p_r_f(self) -> ResultPRF:
        '''Calculate P/R/F1, maybe print, and return the values'''
        result = ResultPRF()
        result.calculate_p_r_f(self)
        return result

######################################################################
######################################################################

# list of (T, P, TP) for the metrics of a sentence, ordered by "self.count"
# E.g. first element corresponds to "Global", second to "Continuous", etc.
MetricCounter = List[MatchCounter]
# list of metric counters for each sentence of the pred/comp corpus
CorpusCounter = List[MetricCounter]

class Evaluator(object):
    """
    TODO: add doc
    """

    def __init__(self, args):
        """
        TODO: add doc
        """
        self.args = args
        self.lemma_surf_map = None
        self.seen = None # initialise only once and keep in object
        self.gold = None # initialise only once and keep in object
        self.counts =  \
                 [("MWE-based", lambda m, s: True),
                  ("Continuous", lambda m, s: m.n_gaps() == 0),
                  ("Discontinuous", lambda m, s: m.n_gaps() != 0),
                  ("Multi-token", lambda m, s: len(m.span) > 1),
                  ("Single-token", lambda m, s: len(m.span) == 1)]
        self.seencounts =  \
                  [("Seen-in-train", self.is_seen ),
                  ("Unseen-in-train", lambda m, s: not self.is_seen(m, s)),
                  ("Variant-of-train", lambda m, s: self.is_seen(m, s) and \
                                       not self.is_ident(m, s)),
                  ("Identical-to-train", lambda m, s: self.is_seen(m, s) and \
                                         self.is_ident(m, s))]
        # Gold = test.cupt; Pred = test.system.cupt
        if "test.cupt" in self.args.pred_file.name or \
           "system" in self.args.gold_file.name:
            cupt.warn("Something looks wrong in gold & system arguments.\n"\
                      "Is `{gold_file.name}` really the gold test.cupt file?\n"\
                      "Is `{pred_file.name}` really a system prediction file?",
                      gold_file=self.args.gold_file,
                      pred_file=self.args.pred_file)

######################################################################

    def sent_stats(self, sent_gold: TokenList, sent_pred: TokenList,
                       countname: str,
                       fctfilter: Callable[[cupt.MWE, TokenList], bool] \
                       = lambda m, s: True) -> MatchCounter:
        """
        TODO: add doc
        """
        mwes_gold = self.to_tokset(cupt.retrieve_mwes(sent_gold).values(),
                                   sent_gold, fctfilter)
        mwes_pred = self.to_tokset(cupt.retrieve_mwes(sent_pred).values(),
                                   sent_pred, fctfilter)
        return MatchCounter(countname, len(mwes_gold), len(mwes_pred),
                            len(mwes_gold & mwes_pred))

######################################################################

    def to_tokset(self, mwes: Iterable[cupt.MWE], sent: TokenList,
                  fctfilter: Callable[[cupt.MWE, TokenList], bool] \
                  = lambda x, s: True) -> Set[FrozenSet[int]]:
        """Return a set of MWEs, each one represented as a frozenset of
        integers (span). MWEs are ordered in an "arbitary" order (but in
        practice, we sort it, for human readability).
        NOTE: we group identical MWEs as a single unit, as per the Shared Task
        meeting's discussion regarding a set-based definition of MWEs.
        """
        tokset = set(frozenset(i for i in m.span) for m in mwes \
                                                  if fctfilter(m, sent))
        return set(sorted(tokset, key=lambda tokset: list(sorted(tokset))))

######################################################################

    def extract_seen_mwes(self, train_file: argparse.FileType) -> \
                           Dict[str, Set[str]]:
        """
        TODO: add doc
        """
        seen: Dict[str, Set[str]] = {}
        for sentence in parse_incr(train_file):
            for (lemma, form) in cupt.retrieve_mwe_lemmaform_map(sentence):
                seen[lemma] = seen.get(lemma, set([]))
                seen[lemma].add(form)
        return seen

######################################################################

    def is_seen(self, mwe: cupt.MWE, sent: TokenList) -> bool :
        """
        TODO: add doc
        """
        return mwe.lemmanorm(sent) in self.seen

######################################################################

    def is_ident(self, mwe: cupt.MWE, sent: TokenList) -> bool :
        """
        TODO: add doc
        """
        return mwe.formseq(sent) not in self.seen[mwe.lemmanorm(sent)]

######################################################################

    def get_tp_stats(self, gold: Deque[TokenList], \
                     pred: Deque[TokenList]) -> CorpusCounter:
        """
        TODO: add doc
        """
        new_gold : Deque[TokenList] = collections.deque() #don't throw gold away
        matches_pred : CorpusCounter = []
        while gold or pred:
            self.check_eof(gold, pred)
            sent_gold = gold.popleft()
            new_gold.append(sent_gold)
            sent_pred = pred.popleft()
            matches_pred.append([])
            for (count, fctfilter) in self.counts :
                match_pred = self.sent_stats(sent_gold, sent_pred,
                                             count, fctfilter)
                matches_pred[-1].append(match_pred)
                # optimisation, no need to calculate other stats if global is
                # null: no other measure will be different from null
                if count == "MWE-based" and not match_pred :
                    break # stop calculating other metrics
        self.gold = new_gold
        return matches_pred


######################################################################

    def aggregate_matches(self, matches: CorpusCounter) :
        """
        TODO: add doc
        """
        accums = [MatchCounter(count) for (count, fct) in self.counts]
        for metric_counter in matches :
            for (i_m, match_counter) in enumerate(metric_counter) :
                if match_counter :
                    accums[i_m] += match_counter # accummulate
        return accums

######################################################################

    def calculate_metrics(self, pred_aggreg: List[MatchCounter]) \
                          -> List[ResultPRF]:
        """
        TODO: add doc
        """
        pred_results = []
        for match in pred_aggreg :
            pred_result = match.get_p_r_f()
            pred_results.append(pred_result)
        return pred_results

######################################################################

    def evaluate_prf_metrics(self, gold_file: TextIO, pred_file: TextIO,
                             train_file: Optional[TextIO] = None) \
                             -> Tuple[CorpusCounter, List[ResultPRF], List[MatchCounter]]:
        """
        TODO: add doc
        """
        if train_file and not self.seen: # if self.seen, do not extract again
            self.seen = self.extract_seen_mwes(self.args.train_file)
            self.counts.extend(self.seencounts) # Also calculate these measures
        if not self.gold :
            self.gold = collections.deque(parse_incr(gold_file))
        pred = collections.deque(parse_incr(pred_file))
        pred_tpstats = self.get_tp_stats(self.gold, pred)
        pred_aggreg = self.aggregate_matches(pred_tpstats)
        pred_results = self.calculate_metrics(pred_aggreg)
        return (pred_tpstats, pred_results, pred_aggreg)

######################################################################

    def create_sample(self, pred_tpstats: CorpusCounter,
                      comp_tpstats: CorpusCounter, samp_size:int) \
                      -> Tuple[CorpusCounter, CorpusCounter]:
        """
        TODO: add doc

        Notice that the result may be smaller than sample_size, if
        some selected sentences happend to be empty (in both pred and comp).
        """
        pred_sample_tpstats = [MatchCounter(count) for (count,f) in self.counts]
        comp_sample_tpstats = [MatchCounter(count) for (count,f) in self.counts]
        for i_sentence in range(samp_size) :
            rand_i = random.randrange(0,samp_size)
            pred_rand = pred_tpstats[rand_i]
            comp_rand = comp_tpstats[rand_i]
            # ignore sentences with empty first element [0] - empty
            if pred_rand[0] or comp_rand[0] :
                for (i_match, pred_match) in enumerate(pred_rand) :
                    pred_sample_tpstats[i_match] += pred_match
                for (i_match, comp_match) in enumerate(comp_rand) :
                    comp_sample_tpstats[i_match] += comp_match
        return (pred_sample_tpstats,comp_sample_tpstats)


######################################################################

    def greater(self, thresh:float, sample_value:float) -> int:
        """
        TODO: add doc
        """
        #print(sample_value,2*thresh)
        if thresh < 0 : # meaning that comp is actually better than pred
            return 1 if sample_value < 2 * thresh else 0
        elif  thresh > 0: # usual test: is pred stat. sig. better than comp?
            return 1 if sample_value > 2 * thresh else 0
        else: # no difference between the original results, likely an error
            return 1

######################################################################

    def check_deltas(self, delta:List[ResultPRF]):
        """
        TODO: add doc
        """
        scorenames = ["Precision", "Recall", "F1"]
        something_strange = False
        for (i, (count, fct)) in enumerate(self.counts) :
            for (j, scorename) in enumerate(scorenames) :
                if delta[i][j] == 0 :
                    cupt.warn("{} {} identical scores".format(count, scorename))
                    something_strange = True
        if something_strange :
            cupt.warn("Maybe this is a coincidence (!) but it's worth checking")

######################################################################


    def pvalue_bootstrap(self, pred_tpstats: CorpusCounter,
                         comp_tpstats: CorpusCounter,
                         pred_results: List[ResultPRF],
                         comp_results:  List[ResultPRF],
                         bootstrap_samples: int = DEFAULT_BOOTSTRAP_N,
                         output: bool = False) -> List[ResultPRF]:
        """
        TODO: add doc
        Returns a list of metrics, each containing the p-value for P/R/F1
        """
        delta = [pred - comp for (pred,comp) in zip(pred_results, comp_results)]
        self.check_deltas(delta)
            #pdb.set_trace()
        # counter for the nb. of times we observe a difference higher than 2 * delta
        nb_sig_samp = [[0, 0, 0] for (c,f) in self.counts]
        # Create a single list of tuples from which to sample
        samp_size = len(pred_tpstats)
        for i_bootstrap in range(bootstrap_samples) :
            (pred_sample_tpstats,comp_sample_tpstats) = \
                    self.create_sample(pred_tpstats, comp_tpstats, samp_size)
            pred_samp_res = self.calculate_metrics(pred_sample_tpstats)
            comp_samp_res = self.calculate_metrics(comp_sample_tpstats)
            delta_samp = [p - c for (p,c) in zip(pred_samp_res, comp_samp_res)]
            for i_s in range(len(self.counts)): # range over metrics
                for i_m in range(3) : # range over P/R/F1 counters
                    diff = self.greater(delta[i_s][i_m], delta_samp[i_s][i_m])
                    nb_sig_samp[i_s][i_m] += diff
        result = []
        for i_s in range(len(self.counts)): # range over metrics
            for i_m in range(3) : # range over P/R/F1 counters
                nb_sig_samp[i_s][i_m] /= bootstrap_samples
            result.append(ResultPRF(*nb_sig_samp[i_s]))
        return result

######################################################################

    def print_mwebased_proportion(self, prefix: str, focused:MatchCounter, 
                                  base: MatchCounter):
        r'''Print proportion of focused/base.'''
        n_gold_self = focused.total_gold
        n_gold_baseline = base.total_gold
        n_pred_self = focused.total_pred
        n_pred_baseline = base.total_pred
        print("* {}MWE-proportion: gold={}/{}={:.0%} pred={}/{}={:.0%}".format(prefix,
              n_gold_self, n_gold_baseline, n_gold_self/(n_gold_baseline or 1),
              n_pred_self, n_pred_baseline, n_pred_self/(n_pred_baseline or 1)))

######################################################################

    def print_eval_header(self,rprf:ResultPRF):      
        print(rprf.row_spacing("","Precision","Recall","F-score"))

######################################################################

    def output_results(self, pred_results: List[ResultPRF],
                       comp_results: List[ResultPRF],
                       pred_matches: List[MatchCounter],
                       comp_matches: List[MatchCounter],
                       pvalues: List[ResultPRF]):
        """
        TODO: add doc
        """
        zipped = zip(self.counts, pred_results, comp_results, 
                     pred_matches, comp_matches, pvalues)
        for (count, pred, comp, pmatch, cmatch, pval) in zipped :
            #pdb.set_trace()

            if count[0] == "MWE-based" :
                print("## Global evaluation")
                self.print_eval_header(pred)
            elif count[0] == "Continuous" :
                print("\n## MWE continuity (partition of Global)")
            elif count[0] == "Multi-token" :
                print("\n## Number of tokens (partition of Global)")
            print(pred.str_detail(pmatch, "pred"))
            if comp :
              print(comp.str_detail(cmatch, "comp"))
              print("p-value={}".format(pval))

######################################################################


    def run(self):
        """
        TODO: add doc
        """
        # Calculate true-positive counts per sentence + p/r/f1 results
        # The former are used to calculate the significance using bootstrapping
        (pred_tpstats, pred_results, pred_matches) = \
            self.evaluate_prf_metrics(self.args.gold_file,
                                      self.args.pred_file,
                                      self.args.train_file)        
        if self.args.comp_file :
            (comp_tpstats, comp_results, comp_matches) = \
            self.evaluate_prf_metrics(self.args.gold_file,
                                      self.args.comp_file,
                                      self.args.train_file)
            pvalues = self.pvalue_bootstrap(pred_tpstats, comp_tpstats,
                                            pred_results, comp_results,
                                            bootstrap_samples=self.args.bootstrap_samples)
        else :
            pvalues = comp_results = comp_matches = [None] * len(pred_results)
        self.output_results(pred_results, comp_results, 
                            pred_matches, comp_matches, pvalues)

######################################################################

    def check_eof(self, gold: Deque, pred: Deque):
        """Generate an error if one file is at EOF and the other is not."""
        if not gold:
            error("Gold file smaller than pred (or comp), please check!")
        if not pred:
            error("Pred (or comp) file smaller than gold, please check!")

######################################################################
######################################################################

def error(message, **kwargs):
    """Print error message and quit."""
    cupt.warn(message, warntype="ERROR", **kwargs)
    sys.exit(1)

######################################################################

if __name__ == "__main__":
    Evaluator(parser.parse_args()).run()
